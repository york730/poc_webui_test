import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('001 Login to HR account'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Navigate to User Activity Report'), [:], FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('Page_AMPOS Web Dashboard/a_Report'))

not_run: WebUI.click(findTestObject('Object Repository/Page_AMPOS Web Dashboard/a_Users Activity Report'))

not_run: WebUI.navigateToUrl(GlobalVariable.url + '/manage/reports/user-activity')

WebUI.click(findTestObject('Object Repository/Page_AMPOS/input_Filter by_startDate'))

WebUI.setText(findTestObject('Object Repository/Page_AMPOS/input_Filter by_startDate'), '09/01/2019')

not_run: WebUI.click(findTestObject('Object Repository/Page_AMPOS/input_Filter by_endDate'))

not_run: WebUI.setText(findTestObject('Object Repository/Page_AMPOS/input_Filter by_endDate'), '12/31/2019')

'confirm EndDate'
WebUI.click(findTestObject('Object Repository/Page_AMPOS/td_1'))

WebUI.click(findTestObject('Page_AMPOS/button_User_Activity_Report_Run'))

